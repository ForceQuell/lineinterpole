//LineInterpole
//main.cpp

//v1.0

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <getopt.h>

using namespace std;

class point2d {
private:
	double x, y;

public:
	void setX(double v) { x = v; }
	void setY(double v) { y = v; }
	void print() { cout << x << "\t" << y << endl; }
	double getX() { return x; }
	double getY() { return y; }
};

class line2d {
private:
	double k, b;
	point2d p1, p2;

public:
	void findParams(point2d p1_, point2d p2_) {
		k = ( p2_.getY() - p1_.getY() ) / ( p2_.getX() - p1_.getX() );
		b = p1_.getY() - k * p1_.getX();
		p1 = p1_;
		p2 = p2_;

		/*p1_.print();
		p2_.print();
		cout << "\t\tk: " << k << "  b: " << b << endl << endl;*/
	}

	double calcY(double x) {
		return k * x + b;
	}
};

class pointKit{
private:
	vector<point2d> data;
	vector<line2d> lData;
	double minX, maxX, minY, maxY;
	string fileName;

public:
	void setData(char* path) {

		fileName = path;

		int n = 0;
		double d;
		point2d p;

		cout << "\tfile: " << path;

		ifstream file;
		file.open(path);

		if (!file) {
			cout << "\tfile was not open!" << endl;
			exit(-1);
		}
		else
			cout << "\tis opened" << endl;

		while (!file.eof()) {
			file >> d;
			n++;
		}

		if (n % 2)
			n--;

		double** arr = new double*[n / 2];
		for (int i = 0; i < n / 2; i++)
			arr[i] = new double[2];

		file.clear();
		file.seekg(0);

		vector<point2d> ret;

		for (int i = 0; i < n / 2; i++) {
			file >> d;
			p.setX(d);
			file >> d;
			p.setY(d);
			ret.push_back(p);
		}
		file.close();

		data = ret;
	}

	void findMaxMin() {
		minX = data[0].getX();
		minY = data[0].getY();
		maxX = data[0].getX();
		maxY = data[0].getY();

		for (int i = 0; i < data.size(); i++) {
			if (data[i].getX() < minX)
				minX = data[i].getX();
			else if (data[i].getX() > maxX)
				maxX = data[i].getX();
			if (data[i].getY() < minY)
				minY = data[i].getY();
			else if (data[i].getY() > maxY)
				maxY = data[i].getY();
		}
	}

	void findLData(char* path) {
		line2d l;

		setData(path);
		findMaxMin();

		for (int i = 0; i < data.size() - 1; i++) {
			l.findParams(data[i], data[i+1]);
			lData.push_back(l);
		}

	}

	double findY(double x) {
		int i;

		if (x > maxX || x < minX)
			return NULL;

		for (i = 0; i < data.size(); i++)
			if (x == data[i].getX())
				return data[i].getY();

		i = 0;
		while ( !(x > data[i].getX() && x < data[i+1].getX()) )
			i++;

		return lData[i].calcY(x);

	}

	void printData() {
		for (auto s : data)
			cout << s.getX() << "\t" << s.getY() << endl;

	}

	vector<point2d>& getData() { return data; }
	double getMinX() { return minX; }
	double getMaxX() { return maxX; }
	double getMinY() { return minY; }
	double getMaxY() { return maxY; }

	pointKit(char* path) {
		findLData(path);
	}
};

string version = "v1.0";
string filePath = "default.txt", outputPath = "defaultOut.txt";
double s = 0.1, t = NULL, e = NULL;

void keyParse(int argc, char** argv) {
	const char* opts = "f:o:s:t:e:";
	int opt;

	while ((opt = getopt(argc, argv, opts)) != -1) {
		switch (opt) {
		case 'f':
			filePath = optarg;
			break;
		case 'o':
			outputPath = optarg;
			break;
		case 's':
			s = atof(optarg);
			break;
		case 't':
			t = atof(optarg);
			break;
		case 'e':
			e = atof(optarg);
			break;
		}
	}

}

int main(int argc, char** argv) {
	cout << "LineInterpole \tversion: " << version << endl << endl;
	ofstream file;

	keyParse(argc, argv);
	file.open(outputPath);

	if (!file) {
		cout << "    [error while creating output file] " << endl;
		exit(-1);
	}

	pointKit PK((char*)filePath.data());
	cout << "\tfile: " << outputPath << "\tcreated" << endl;
	if (t == NULL)
		t = PK.getMinX();
	if (e == NULL)
		e = PK.getMaxX();

	for (; t <= e; t+=s)
		file << t << "\t\t\t" << PK.findY(t) << endl;

	file.close();
	return 0;
}

